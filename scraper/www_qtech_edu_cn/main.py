from scraper.utils.html import *

base_url = 'http://www.qtech.edu.cn'
save_path = 'scraper/www_qtech_edu_cn/content'

normal_urls = {
    # 校情总览
    '学校概况': 'http://www.qtech.edu.cn/xqzl/xxgk.htm',
    '历史沿革': 'http://www.qtech.edu.cn/xqzl/lsyg.htm',
    '历任领导': 'http://www.qtech.edu.cn/xqzl/lrld.htm',
    '现任领导': 'http://www.qtech.edu.cn/xqzl/xrld.htm',
    '学校标志': 'http://www.qtech.edu.cn/xqzl/xxbz.htm',
    '临沂校区': 'http://www.qtech.edu.cn/xqzl/lyxq.htm',

    # 组织结构
    '组织机构': 'http://www.qtech.edu.cn/zzjg.htm',

    # 院部设置
    '院部设置': 'http://www.qtech.edu.cn/ybsz1.htm',

    # 师资队伍
    '师资概况': 'http://www.qtech.edu.cn/szdw/szgk.htm',
    '名师风采': 'http://www.qtech.edu.cn/szdw/msfc.htm',

    # 教育教学
    '教育教学': 'http://www.qtech.edu.cn/jyjx.htm',

    # 科学研究
    # 注意： 科研项目、科研成果、学术期刊尚未处理
    '科学研究': 'http://www.qtech.edu.cn/kxyj.htm',
    '科研机构': 'http://www.qtech.edu.cn/kxyj/kyjg.htm',

    # 招生就业
    # 注意： 只爬取的主页，详细条目未作处理
    '招生就业': 'http://www.qtech.edu.cn/zsjy.htm',

    # 合作交流
    # 注意： 同样是独立站点，先不作处理

    # 公共服务
    # 注意： 只爬取的主页，详细条目未作处理
    '公共服务': 'http://www.qtech.edu.cn/ggfw.htm',
}

listed_urls = {
    # 校园生活
    '校园生活': 'http://www.qtech.edu.cn/xysh.htm',
    '社团活动': 'http://www.qtech.edu.cn/xysh/sthd.htm',
    '社会实践': 'http://www.qtech.edu.cn/xysh/shsj.htm',
}


# 根据分页器获取页面总数
# 参数 bs4对象
# 数字 页面总数
def get_page_num(html):
    pagination = html.find(id='fanye196645')
    start_index = re.search(r'/\d+', pagination.text).span()[0]
    return int(pagination.text[start_index + 1:])


# 爬虫执行爬取操作的启动函数
# 返回 字符串
def get_normal(url_list):
    text = ''

    for (name, article) in url_list.items():
        print('正在爬取: %s' % name)
        text += parse(get_html(article).find(class_='ernyouc880'))

    return text


def get_listed_articles(start_url):
    page_list = parse_pagination('%s/' % start_url.split('.htm')[0], '.htm', get_page_num(get_html(start_url)))
    page_list.append(start_url)

    for page_url in page_list:
        page_content = ''
        article_list = get_html(page_url).find_all(class_='lieblk')
        for article_url in get_article_urls(article_list):
            print('正在爬取: %s' % article_url)
            page_content += parse(get_html('%s/%s' % (base_url, article_url)).find(class_='jan1200'))

        with open('%s/%s.txt' % (save_path, re.sub('/', '_', page_url)), 'w') as page_file:
            print('保存: %s.txt' % page_url)
            page_file.write(page_content)


normal_content = ''
normal_content += get_normal(normal_urls)
with open('%s/normal_content.txt' % save_path, 'w') as file:
    print('保存: normal_content.txt')
    file.write(normal_content)

for (key, url) in listed_urls.items():
    print('正在爬取: %s' % key)
    get_listed_articles(url)

print('爬取完毕！')
