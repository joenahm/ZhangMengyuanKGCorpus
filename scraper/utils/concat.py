import sys
import os

# 本文件实现指定目录下txt文件合并功能
# 命令行参数为目录地址

if len(sys.argv) > 1:
    for (root, _, files) in os.walk(sys.argv[1]):
        content = ''
        for name in files:
            with open('%s/%s' % (root, name), 'r') as file:
                content += file.read()
        with open('%s.txt' % root, 'w') as all_content:
            all_content.write(content)

        print('%s.txt: 文件合并完成！' % root)
else:
    print("[ 错误 ]: 请指定合并地址！")
