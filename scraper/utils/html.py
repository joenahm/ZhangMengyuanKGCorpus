# 此文件是爬虫所需html解析相关的函数库
import requests
from bs4 import BeautifulSoup
import re


# 通过bs4获取网页html文档
# 参数: url 地址
# 返回： bs4对象
def get_html(url):
    r = requests.get(url)
    r.encoding = 'utf-8'
    bs = BeautifulSoup(r.content, 'html.parser')
    return bs


# 解析页面，提取文本
# 参数 target bs4筛选出的含有内容的元素标签
# 返回 字符串
def parse(target):
    if target:
        content = ''

        for tag in target.find_all('script'):
            tag.clear()

        content += '%s\n' % re.sub(r'[\s]', '', target.text)

        return content
    else:
        return None


# 根据分页总数取得全部列表页url
# 参数
# prefix 页面地址中数字前的部分
# suffix 页面地址中数字后的部分
# count 页面总数
# 返回 列表：列表页url
def parse_pagination(prefix, suffix, count):
    page_list = []
    for i in range(count-1):
        page_list.append('%s%d%s' % (prefix, i + 1, suffix))

    return page_list


# 从列表页提取出全部文章url
# 参数 article_list 文章bs4对象的列表
# 返回 列表：文章url
def get_article_urls(article_list):
    url_list = []
    for item in article_list:
        if item.find('a'):
            a_node = item.find('a')
        else:
            a_node = item
        url_list.append(a_node.attrs['href'])

    return url_list
