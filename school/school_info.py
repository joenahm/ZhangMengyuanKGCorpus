from py2neo import Graph, Node, Relationship, NodeMatcher
import pandas as pd
import re

qut_graph = Graph(
    'http://localhost:7474',
    username='neo4j',
    password='00001011'
)

matcher = NodeMatcher(qut_graph)

school_node = matcher.match('学院', name='高等职业学院').first()

people = pd.read_csv('高等职业学院 .csv')
for _, person in people.iterrows():
    print(person['姓名'], person['职务'])

    person_node = Node('人', name=person['姓名'])

    # 处理多职务情况
    split_arr = re.split('、', person['职务'])
    if len(split_arr) > 1:
        for position in split_arr:
            qut_graph.create(Relationship(person_node, position, school_node))
    else:
        qut_graph.create(Relationship(person_node, person['职务'], school_node))
